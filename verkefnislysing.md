# launagreining.is #

### Lýsing ###
Til þess að bjóða upp á fyrirtækjalausn fyrir launagreiningu ætlum við að búa til vefsíðu sem býður upp á að skrá sig inn og hlaða inn skjali til greiningar og getum síðan birt niðurstöðurnar, kerfið geymir niðurstöðurnar.

### Kröfur um virkni ###

*Almennur notandi*
- Getur nýskráð sig
- Getur skráð sig inn
- Getur séð almennar upplýsingar um hvernig greiningin fer fram

*Innskráður notandi*
- Getur upphlaðið Excel-skjali til greiningar
- Getur skoðað gröf með kyn á öðrum ásnum og laun á hinum

*Ýmsir möguleikar*
- Að síðan styðji fleiri en eitt tungumál
- Geta niðurhalað niðurstöðum á (excel-skjal, csv)
